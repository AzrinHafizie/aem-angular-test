import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'azrin-tagging',
  templateUrl: './tagging.component.html',
  styleUrls: ['./tagging.component.scss']
})
export class TaggingComponent implements OnInit {

  @Input('value') valueArray: string;
  @Output() removed = new EventEmitter();
  @Output() addValue = new EventEmitter();

  newValue;
  inputWidth = '30px';

  constructor() { }

  ngOnInit() {
  }

  remove(value, i){
    // this.removed.emit({skill: skill, index: i});
    this.removed.emit(i);
  }

  addNewValue(value){
    this.newValue = '';
    this.inputWidth = '30px';
    this.addValue.emit(value);
  }

  resizeWidth(char){
    // console.log(char);
    if(char){
      this.inputWidth = (char.length*8)+30+'px';
    }
    // console.log(this.inputWidth);
  }

  backspaceRemove(char){
    // console.log(char)
    if(!char || char.length == 0){
      let idx = this.valueArray.length - 1;
      let value = this.valueArray[idx];
      this.remove(value, idx)    
    }
  }

}
