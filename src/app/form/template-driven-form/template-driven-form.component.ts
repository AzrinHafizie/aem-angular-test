import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.scss']
})
export class TemplateDrivenFormComponent implements OnInit {

  user

  constructor() { }

  ngOnInit() {
    this.initUser()
  }

  initUser(){
    this.user = {
      name: {
        first: '',
        last: ''
      },
      email: '',
      phone: {
        mobile: ''
      },
      skills: [
        'skill 1',
        'skill 2'
      ]
    };
  }

  removeSkill(idx){
    // console.log(idx);
    this.user.skills.splice(idx, 1);
  }

  onAddSkill(skill){
    // console.log(skill);
    this.user.skills.push(skill)
  }

}
