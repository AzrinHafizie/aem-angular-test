import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormRouting } from './form.routing';
import { FormPage } from './form.page';

import { TaggingComponent } from './../shared/tagging/tagging.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { TemplateDrivenFormComponent } from './template-driven-form/template-driven-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormRouting
  ],
  declarations: [
    FormPage,
    TaggingComponent,
    ReactiveFormComponent,
    TemplateDrivenFormComponent
  ]
})
export class FormModule { }
